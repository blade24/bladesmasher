package com.blade.bladesmasher.monster.abstractfactory;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Array;
import com.blade.bladesmasher.monster.MonsterState;
import com.blade.bladesmasher.monster.observer.MonsterEvent;
import com.blade.bladesmasher.monster.observer.MonsterObserver;
import com.blade.bladesmasher.monster.observer.MonsterSubject;

/**
 * ����������� �����, ������� ��������� ������� ��������� ������� (Abstract object)
 * @author blade
 *
 */
public abstract class IMonster extends Actor implements Runnable, MonsterSubject {
	
	protected Animation walk;
	protected Animation death;
	protected TextureRegion[] walkFrames;
	protected TextureRegion[] deathFrames;
	protected MonsterState monsterState;
	protected float stateTime;
	protected Array<MonsterObserver> monsterObservers = new Array<MonsterObserver>();
	protected com.blade.bladesmasher.monster.observer.MonsterEvent event;
	
	public abstract void initMonsterWalkAnimation();
	public abstract void initMonsterDeathAnimation();
	public abstract int getScoreForMonster();
	public abstract float getMosnterSpeed();
	
	public IMonster() {
		stateTime = 0;
		initMonsterWalkAnimation();
		initMonsterDeathAnimation();
		monsterState = MonsterState.WALKING;	
	}
	
	public MonsterState getState() {
		return monsterState;
	}
	
	public void setMonsterState(MonsterState state) {
		this.monsterState = state;
	}
	
	@Override
	public void run() {
		free();
	}
	
	public void stopAnimation() {
		walk.setPlayMode(Animation.NORMAL);
	}
	
	public void continueAnimation() {
		walk.setPlayMode(Animation.LOOP_PINGPONG);
	}
	
	public void die() {
		this.event = MonsterEvent.DIED;
		notifyMonsterObservers();
	}
	
	public void free() {
		this.remove();
		this.event = MonsterEvent.OUT_OF_RANGE;
		notifyMonsterObservers();
	}

	public void increaseMonsterStateTime(float stateTime) {
		this.stateTime += stateTime;
	}
	
	protected float getStateTime() {
		return stateTime;
	}
	
	@Override
	public void addMonsterObserver(MonsterObserver observer) {
		monsterObservers.add(observer);
	}

	@Override
	public void removeMonsterObserver(MonsterObserver observer) {
		monsterObservers.removeValue(observer, true);
	}

	@Override
	public void notifyMonsterObservers() {
		for(MonsterObserver mObserver : monsterObservers) {
			mObserver.monsterEventHappened(this, event);
		}
	}
	
	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		batch.setColor(getColor());
		TextureRegion frame = null;
		//Thread.sleep(100);
		switch (getState()) {
			case WALKING:
				frame = walk.getKeyFrame(getStateTime());
				break;
			case DYING:
				frame = death.getKeyFrame(getStateTime());
				break;
		}
		batch.draw(frame, getX(), getY(), getWidth(), getHeight());
	}
}

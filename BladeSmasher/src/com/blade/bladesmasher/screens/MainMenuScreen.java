package com.blade.bladesmasher.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.blade.bladesmasher.actors.Sprite;
import com.blade.bladesmasher.actors.MenuPanel;

public class MainMenuScreen implements Screen{
	private Stage stage;
	public MainMenuScreen(SpriteBatch batch) {
        stage = new Stage(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), false, batch);
        initBackground(new TextureRegion(new Texture("data/dirt_road.jpg")));
        initGameTitleImage(new TextureRegion(new Texture("data/title.png")));
        initMainMenu();
    }
	
	@Override
	public void render(float delta) {
		// TODO Auto-generated method stub
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
	}
	
	private void initGameTitleImage(TextureRegion textureRegion) {
		Sprite gameTitle = new Sprite(textureRegion);
		gameTitle.setPosition(Gdx.graphics.getWidth()*0.082f, Gdx.graphics.getHeight()*0.700f);
		gameTitle.setSize( Gdx.graphics.getWidth()*0.850f, Gdx.graphics.getHeight()*0.170f);
		stage.addActor(gameTitle);
	}
	
	private void initMainMenu() {
		MenuPanel menuPanel = new MenuPanel(-25,0);
		menuPanel.setPosition(Gdx.graphics.getWidth()/3, Gdx.graphics.getHeight()/3);
		stage.addActor(menuPanel);
	}
	
	private void initBackground(TextureRegion textureRegion) {
		Sprite backgroundActor = new Sprite(textureRegion);
		backgroundActor.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		stage.addActor(backgroundActor);
	}
	
	@Override
	public void resize(int width, int height) {
		
	}
	
	@Override
	public void show() {
		Gdx.input.setInputProcessor(stage);
	}
	
	@Override
	public void hide() {
		
	}
	
	@Override
	public void pause() {
		
	}
	
	@Override
	public void resume() {
		
	}
	
	@Override
	public void dispose() {
		
	}

}

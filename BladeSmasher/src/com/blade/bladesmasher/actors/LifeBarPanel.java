package com.blade.bladesmasher.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Group;

public class LifeBarPanel extends Group {
	
	private Sprite heart1, heart2, heart3;
	private static final float ICON_SIZE = Gdx.graphics.getWidth()*0.050f;
	public LifeBarPanel(float posX, float posY) {
		heart1 = new Sprite(new TextureRegion(new Texture("data/life-heart.png")));
		heart1.setSize(ICON_SIZE, ICON_SIZE);
		heart1.setPosition(posX, posY);
		addActor(heart1);
		
		heart2 = new Sprite(new TextureRegion(new Texture("data/life-heart.png")));
		heart2.setSize(ICON_SIZE, ICON_SIZE);
		heart2.setPosition(heart1.getX()+heart2.getWidth(), posY);
		addActor(heart2);
		
		heart3 = new Sprite(new TextureRegion(new Texture("data/life-heart.png")));
		heart3.setSize(ICON_SIZE, ICON_SIZE);
		heart3.setPosition(heart2.getX()+heart3.getWidth(), posY);
		addActor(heart3);
	}
	
	public void removeHeart1() {
		removeActor(heart1);
	}
	
	public void removeHeart2() {
		removeActor(heart2);
	}
	
	public void removeHeart3() {
		removeActor(heart3);
	}
}

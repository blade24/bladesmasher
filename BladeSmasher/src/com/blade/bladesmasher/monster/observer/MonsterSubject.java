package com.blade.bladesmasher.monster.observer;

public interface MonsterSubject {
	public void addMonsterObserver(MonsterObserver observer);
	public void removeMonsterObserver(MonsterObserver observer);
	public void notifyMonsterObservers();
}

package com.blade.bladesmasher.monster.observer;

import com.blade.bladesmasher.monster.abstractfactory.IMonster;

public interface MonsterObserver {
	public void monsterEventHappened(IMonster monster, MonsterEvent event);
}

package com.blade.bladesmasher.actors;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.blade.bladesmasher.BladeSmasher;

public class MenuPanel extends Group {
	private MenuItem startButton;
	private MenuItem settingsButton;
	private MenuItem exitButton;
	public MenuPanel(float posX, float posY) {
		startButton = new MenuItem("Start Game");
		startButton.setSize(MenuItem.WIDTH, MenuItem.HEIGHT);
		startButton.setPosition(posX, posY);
		startButton.addListener(new StartButtonClickListener());
		addActor(startButton);
		
		settingsButton = new MenuItem("Settings");
		settingsButton.setSize(MenuItem.WIDTH, MenuItem.HEIGHT);
		settingsButton.setPosition(posX, startButton.getY()+startButton.getHeight()+settingsButton.getHeight()/5f);
		addActor(settingsButton);
		
		exitButton = new MenuItem("Exit");
		exitButton.setSize(MenuItem.WIDTH, MenuItem.HEIGHT);
		exitButton.setPosition(posX, startButton.getY()-startButton.getHeight()-exitButton.getHeight()/5f);
		addActor(exitButton);
	}
	
	class StartButtonClickListener extends InputListener {
		@Override
		public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
			startButton.setScale(0.98f, 0.9f);
			startButton.setLabelColor(Color.RED);
			return true;
		}
		
		@Override
		public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
			startButton.setScale(1f, 1f);
			startButton.setLabelColor(Color.GREEN);
			BladeSmasher.getInstance().showGameTypeSelectionScreen();
		}
	}
}

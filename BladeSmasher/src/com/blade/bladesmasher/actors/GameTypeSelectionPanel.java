package com.blade.bladesmasher.actors;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.blade.bladesmasher.BladeSmasher;
import com.blade.bladesmasher.GameMode;

public class GameTypeSelectionPanel extends Group {
	private MenuItem storyModeButton;
	private MenuItem surviveModeButton;
	private MenuItem timeModeButton;
	public GameTypeSelectionPanel(float posX, float posY) {
		storyModeButton = new MenuItem("Story mode");
		storyModeButton.setSize(MenuItem.WIDTH, MenuItem.HEIGHT);
		storyModeButton.setPosition(posX, posY);
		addActor(storyModeButton);
		
		surviveModeButton = new MenuItem("Survive mode");
		surviveModeButton.setSize(MenuItem.WIDTH, MenuItem.HEIGHT);
		surviveModeButton.setPosition(posX, storyModeButton.getY()+storyModeButton.getHeight()+surviveModeButton.getHeight()/5f);
		surviveModeButton.addListener(new SurviveModeButtonClickListener());
		addActor(surviveModeButton);
		
		timeModeButton = new MenuItem("Time mode");
		timeModeButton.setSize(MenuItem.WIDTH, MenuItem.HEIGHT);
		timeModeButton.setPosition(posX, storyModeButton.getY()-storyModeButton.getHeight()-timeModeButton.getHeight()/5f);
		timeModeButton.addListener(new TimeModeButtonClickListener());
		addActor(timeModeButton);
	}
	
	class StoryModeButtonClickListener extends ClickListener {
		public void clicked(com.badlogic.gdx.scenes.scene2d.InputEvent event, float x, float y) {
			BladeSmasher.getInstance().showGame(GameMode.STORY_MODE);
		};
	}
	
	class SurviveModeButtonClickListener extends InputListener {	
		@Override
		public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
			surviveModeButton.setScale(0.98f, 0.9f);
			return true;
		}
		
		@Override
		public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
			surviveModeButton.setScale(1f, 1f);
			BladeSmasher.getInstance().setGameMode(GameMode.SURVIVE_MODE);
			BladeSmasher.getInstance().showShopScreen();
		}
	}
	
	class TimeModeButtonClickListener extends InputListener {
		@Override
		public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
			timeModeButton.setScale(0.98f, 0.9f);
			return true;
		}
		
		@Override
		public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
			timeModeButton.setScale(1f, 1f);
			BladeSmasher.getInstance().setGameMode(GameMode.TIME_MODE);
			BladeSmasher.getInstance().showShopScreen();
		}
	}
}

package com.blade.bladesmasher.player;

import com.blade.bladesmasher.GameSettings;

/**
 * ����� "�����", ������� ������ ���� � ���������� �����.
 * @author TASHA
 *
 */
public class Player {

	private int money;
	
	private static Player instance = new Player();
	
	public static Player getInstance() {
		if(instance != null) {
			return instance;
		} else {
			return new Player();
		}
	}

	public int getMoney() {
		return money;
	}

	public void setMoney(int money) {
		this.money = money;
	}
	
	public void savePlayer() {
		GameSettings settings = GameSettings.getInstance();
		settings.saveInt("money", money);
		settings.flush();
	}
	
	public void loadPlayer() {
		GameSettings sets = GameSettings.getInstance();
		money = sets.getInt("money", 0);
	}
	
}

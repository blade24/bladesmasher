package com.blade.bladesmasher.monster.factory.concrete;

import com.badlogic.gdx.utils.Pools;
import com.blade.bladesmasher.monster.abstractfactory.IFactory;
import com.blade.bladesmasher.monster.concrete.LeechMonster;

/**
 * ���������� �������, ������� ������� Leech �������� (Concrete factory)
 * @author blade
 *
 */
public class LeechFactory extends IFactory {	
	
	@Override
	public LeechMonster createMonster() {
		return Pools.obtain(LeechMonster.class);
	}
}

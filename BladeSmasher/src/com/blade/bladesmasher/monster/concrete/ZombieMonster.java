package com.blade.bladesmasher.monster.concrete;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.blade.bladesmasher.monster.abstractfactory.IMonster;

/**
 * ���������� ������, ������� ����������� �� �������� IMonster (Concrete object)
 * @author blade
 *
 */
public class ZombieMonster extends IMonster {

	@Override
	public void initMonsterWalkAnimation() {
		Texture monsterSpriteSheetWalk = new Texture("data/zombie-walk.png");
		TextureRegion[] walkRegions = TextureRegion.split(monsterSpriteSheetWalk, 64, 64)[0];
		walkFrames = new TextureRegion[7];
		int index = 0;
        for (int i = 0; i < 7; i++) {
            walkFrames[index++] = walkRegions[i];
        }
		walk = new Animation(0.15f, walkFrames);
		walk.setPlayMode(Animation.LOOP_PINGPONG);
	}
	
	@Override
	public void initMonsterDeathAnimation() {
		Texture monsterSpriteSheetWalk = new Texture("data/zombie-death.png");
		TextureRegion[] deathRegions = TextureRegion.split(monsterSpriteSheetWalk, 64, 64)[0];
		deathFrames = new TextureRegion[7];
		int index = 0;
        for (int i = 0; i < 7; i++) {
        	deathFrames[index++] = deathRegions[i];
        }
		death = new Animation(0.20f, deathFrames);
		death.setPlayMode(Animation.NORMAL);
	}

	@Override
	public int getScoreForMonster() {
		return 7;
	}

	@Override
	public float getMosnterSpeed() {
		return 0;
	}
}

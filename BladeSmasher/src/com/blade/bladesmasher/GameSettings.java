package com.blade.bladesmasher;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

public class GameSettings {
	public static final String PREFERENCES_NAME = "bladesmasher-settings";
	
	private static GameSettings instance = new GameSettings();
	private Preferences prefs;
	
	private GameSettings() {
		prefs = Gdx.app.getPreferences(PREFERENCES_NAME);
		System.out.println(PREFERENCES_NAME);
	}
	
	public static GameSettings getInstance() {
		if (instance == null) {
			instance = new GameSettings();
		}
		return instance;
	}
	
	public Preferences prefs() {
		return prefs;
	}
	
	public GameSettings saveFloat(String name, float value) {
		prefs.putFloat(name, value);
		return this;
	}
	
	public float getFloat(String name, float defValue) {
		return prefs.getFloat(name, defValue);
	}
	
	public String getString(String name, String defValue) {
		return prefs.getString(name, defValue);
	}
	
	public GameSettings saveString(String name, String value) {
		prefs.putString(name, value);
		return this;
	}
	
	public GameSettings flush() {
		prefs.flush();
		return this;
	}

	public GameSettings saveInt(String key, int value) {
		prefs.putInteger(key, value);
		return this;
	}
	
	public int getInt(String key, int defValue) {
		return prefs.getInteger(key, defValue);
	}
	
	public GameSettings saveBool(String key, boolean value) {
		prefs.putBoolean(key, value);
		return this;
	}
	
	public boolean getBool(String key, boolean defValue) {
		return prefs.getBoolean(key, defValue);
	}

	public Preferences getPreferences() {
		return prefs;
	}
}

package com.blade.bladesmasher.monster.abstractfactory;

/**
 * ����������� �������, ������� ������� �������� (Abstract factory)
 * @author blade
 *
 */
public abstract class IFactory {
	
	public abstract IMonster createMonster();
}

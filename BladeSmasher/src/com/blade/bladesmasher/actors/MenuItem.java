package com.blade.bladesmasher.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;

public class MenuItem extends Group {
	public static final float WIDTH = Gdx.graphics.getWidth()*0.5000f;
	public static final float HEIGHT = Gdx.graphics.getWidth()*0.1500f;
	private Label buttonLabel;
	
	public MenuItem(String buttonTitle) {
		Sprite button = new Sprite(new TextureRegion(new Texture("data/menu-button.png")));
		button.setSize(WIDTH, HEIGHT);
		button.setPosition(button.getWidth()*0.04f, button.getY()-HEIGHT/14.3f);
		addActor(button);
		
		buttonLabel =  createLabel(buttonTitle);
		buttonLabel.setPosition(button.getX()+HEIGHT/2, button.getHeight()/3);
		addActor(buttonLabel);

	}
	
	public void setLabelColor(Color color) {
		this.buttonLabel.setColor(color);
	}
	
	private Label createLabel(String text) {
		LabelStyle lStyle = new LabelStyle();
		BitmapFont font = new BitmapFont();
		font.setScale(1.3f);
		lStyle.font = font;
		lStyle.fontColor = Color.GREEN;
		return new Label(text, lStyle);
	}
	
}

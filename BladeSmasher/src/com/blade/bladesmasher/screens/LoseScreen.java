package com.blade.bladesmasher.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.blade.bladesmasher.BladeSmasher;
import com.blade.bladesmasher.actors.Sprite;

public class LoseScreen implements Screen {
	private Stage stage;
	private Label loseLabel;
	public LoseScreen(SpriteBatch batch) {
        stage = new Stage(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), false, batch);
        initBackground(new TextureRegion(new Texture("data/dirt_road.jpg")));
        initLoseLabel();
    }
	 
	@Override
	public void render(float delta) {
 		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
	}
	
	private void initLoseLabel() {
		LabelStyle lStyle = new LabelStyle();
		lStyle.font = new BitmapFont();
		lStyle.fontColor = new Color(1f, 0f,0f, 1f);
		loseLabel = new Label("GAME OVER!", lStyle);
		loseLabel.setPosition(Gdx.graphics.getWidth()/2, Gdx.graphics.getHeight()/2);
		stage.addActor(loseLabel);
	}
	
	private void initBackground(TextureRegion textureRegion) {
		Sprite backgroundActor = new Sprite(textureRegion);
		backgroundActor.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		stage.addActor(backgroundActor);
		stage.addListener(new BackgroundClickListener());
	}
	
	class BackgroundClickListener extends ClickListener {
		public void clicked(com.badlogic.gdx.scenes.scene2d.InputEvent event, float x, float y) {
			System.out.println("background clicked!");
			BladeSmasher.getInstance().showMenu();
		};
	}
	
	@Override
	public void resize(int width, int height) {
		
	}

	@Override
	public void show() {
		Gdx.input.setInputProcessor(stage);
	}

	@Override
	public void hide() {
		
	}

	@Override
	public void pause() {
		
	}

	@Override
	public void resume() {
		
	}

	@Override
	public void dispose() {
		
	}

}

package com.blade.bladesmasher.monster.observer;

public enum MonsterEvent {
	DIED,
	OUT_OF_RANGE
}

package com.blade.bladesmasher.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.blade.bladesmasher.BladeSmasher;
import com.blade.bladesmasher.actors.MenuItem;
import com.blade.bladesmasher.actors.Sprite;
import com.blade.bladesmasher.player.Player;

public class ShopScreen implements Screen {
	private Stage stage;
	private Label moneyLabel;
	private BladeSmasher bsInstance = BladeSmasher.getInstance();
	public ShopScreen(SpriteBatch batch) {
        stage = new Stage(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), false, batch);
        Player.getInstance().loadPlayer();
        initBackground(new TextureRegion(new Texture("data/dirt_road.jpg")));
        initMoneyPanel();
        initStartGameButton();
    }
	
	private void initBackground(TextureRegion textureRegion) {
		Sprite backgroundActor = new Sprite(textureRegion);
		backgroundActor.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		stage.addActor(backgroundActor);
	}
	
	private void initMoneyPanel() {
		LabelStyle lStyle = new LabelStyle();
		lStyle.font = new BitmapFont();		lStyle.fontColor = new Color(1f, 1f,1f, 1f);
		moneyLabel = new Label("Money: "+String.valueOf(Player.getInstance().getMoney()), lStyle);
		moneyLabel.setPosition(Gdx.graphics.getWidth()/2.4f, Gdx.graphics.getHeight()-30);
		stage.addActor(moneyLabel);
	}
	
	private void initStartGameButton() {
		MenuItem startGameBtn = new MenuItem("Play");
		startGameBtn.setSize(MenuItem.WIDTH, MenuItem.HEIGHT);
		startGameBtn.setPosition(Gdx.graphics.getWidth()/4f, 30);
		startGameBtn.addListener(new StartGameClickListener());
		stage.addActor(startGameBtn);
	}
	
	class StartGameClickListener extends ClickListener {
		public void clicked(com.badlogic.gdx.scenes.scene2d.InputEvent event, float x, float y) {
			BladeSmasher.getInstance().showGame(bsInstance.getGameMode());
		};
	}
	
	@Override
	public void render(float delta) {
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		
	}

	@Override
	public void show() {
		Gdx.input.setInputProcessor(stage);
	}

	@Override
	public void hide() {

	}

	@Override
	public void pause() {
		
	}

	@Override
	public void resume() {
		
	}

	@Override
	public void dispose() {
		
	}

}

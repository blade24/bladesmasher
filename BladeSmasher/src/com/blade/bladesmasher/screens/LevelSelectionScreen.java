package com.blade.bladesmasher.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.blade.bladesmasher.actors.Sprite;

public class LevelSelectionScreen implements Screen{
	private Stage stage;
	public LevelSelectionScreen(SpriteBatch batch) {
        stage = new Stage(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), false, batch);
        initBackground(new TextureRegion(new Texture("data/dirt_road.jpg")));
    }
	 
	@Override
	public void render(float delta) {
 		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
	}
	
	private void initBackground(TextureRegion textureRegion) {
		Sprite backgroundActor = new Sprite(textureRegion);
		backgroundActor.setSize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		stage.addActor(backgroundActor);
	}
	
	@Override
	public void resize(int width, int height) {
		
	}

	@Override
	public void show() {
		Gdx.input.setInputProcessor(stage);
	}

	@Override
	public void hide() {
		
	}

	@Override
	public void pause() {
		
	}

	@Override
	public void resume() {
		
	}

	@Override
	public void dispose() {
		
	}
}

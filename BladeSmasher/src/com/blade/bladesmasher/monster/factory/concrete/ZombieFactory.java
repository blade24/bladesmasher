package com.blade.bladesmasher.monster.factory.concrete;

import com.badlogic.gdx.utils.Pools;
import com.blade.bladesmasher.monster.abstractfactory.IFactory;
import com.blade.bladesmasher.monster.concrete.ZombieMonster;
/**
 * ���������� �������, ������� ������� Leech �������� (Concrete factory)
 * @author blade
 *
 */
public class ZombieFactory extends IFactory{

	@Override
	public ZombieMonster createMonster() {
		return Pools.obtain(ZombieMonster.class);
	}
}

package com.blade.bladesmasher;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.blade.bladesmasher.screens.GameScreen;
import com.blade.bladesmasher.screens.GameTypeSelectionScreen;
import com.blade.bladesmasher.screens.LoseScreen;
import com.blade.bladesmasher.screens.MainMenuScreen;
import com.blade.bladesmasher.screens.ShopScreen;

public class BladeSmasher extends Game {
	/**
	 * �������� ����� ����. ��������� �������� ��������. ��������� ������� "Singleton"
	 */
	//���������� ��������
	private SpriteBatch batch; 
    //������� �����
    private GameScreen gameScreen;
    private LoseScreen loseScreen; 
    private MainMenuScreen menuScreen; 
    private ShopScreen shopScreen;
    private GameTypeSelectionScreen gameTypeSelectionScreen;
    private static BladeSmasher instance = new BladeSmasher();
    private GameMode gameMode;
    
    public GameMode getGameMode() {
		return gameMode;
	}

	public void setGameMode(GameMode gameMode) {
		this.gameMode = gameMode;
	}

	private BladeSmasher() {
    	
    }
    
    /**
     * ����� ��������� ������� � ������
     * @return
     */
    public static BladeSmasher getInstance() {
        return instance;
    }
    
	@Override
	public void create() {
		Texture.setEnforcePotImages(false);   
        batch = new SpriteBatch();
        menuScreen = new MainMenuScreen(batch);
        setScreen(menuScreen);
	}
	
    public void showMenu() {
        setScreen(menuScreen);
    }

    public void showGame(GameMode gameMode) {
    	gameScreen = new GameScreen(batch, gameMode);
        setScreen(gameScreen);
    }
    
    public void showGameTypeSelectionScreen() {
    	gameTypeSelectionScreen = new GameTypeSelectionScreen(batch);
    	setScreen(gameTypeSelectionScreen);
    }
    
    public void showLoseScreen() {
    	loseScreen = new LoseScreen(batch);
    	setScreen(loseScreen);
    }
    
    public void showShopScreen() {
    	shopScreen = new ShopScreen(batch);
    	setScreen(shopScreen);
    }
    
}

package com.blade.bladesmasher.monster.concrete;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.blade.bladesmasher.monster.abstractfactory.IMonster;

/**
 * ���������� ������, ������� ����������� �� �������� IMonster (Concrete object)
 * @author blade
 *
 */
public class LeechMonster extends IMonster {

	@Override
	public void initMonsterWalkAnimation() {
		Texture monsterSpriteSheetWalk = new Texture("data/monster-walk.png");
		TextureRegion[] walkRegions = TextureRegion.split(monsterSpriteSheetWalk, 64, 64)[0];
		walkFrames = new TextureRegion[7];
		int index = 0;
        for (int i = 0; i < 7; i++) {
            walkFrames[index++] = walkRegions[i];
        }
		walk = new Animation(0.08f, walkFrames);
		walk.setPlayMode(Animation.LOOP_PINGPONG);
	}
	
	@Override
	public void initMonsterDeathAnimation() {
		Texture monsterSpriteSheetWalk = new Texture("data/monster-death.png");
		TextureRegion[] deathRegions = TextureRegion.split(monsterSpriteSheetWalk, 64, 64)[0];
		deathFrames = new TextureRegion[6];
		int index = 0;
        for (int i = 0; i < 6; i++) {
        	deathFrames[index++] = deathRegions[i];
        }
		death = new Animation(0.15f, deathFrames);
		death.setPlayMode(Animation.NORMAL);
	}

	@Override
	public int getScoreForMonster() {
		return 5;
	}

	/**
	 * ���� ����� ��������
	 */
	@Override
	public float getMosnterSpeed() {
		return 0;
	}
}

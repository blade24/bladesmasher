package com.blade.bladesmasher.screens;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeOut;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveBy;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.rotateBy;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.run;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;
import com.blade.bladesmasher.BladeSmasher;
import com.blade.bladesmasher.GameMode;
import com.blade.bladesmasher.actors.LifeBarPanel;
import com.blade.bladesmasher.actors.Sprite;
import com.blade.bladesmasher.monster.MonsterState;
import com.blade.bladesmasher.monster.abstractfactory.IMonster;
import com.blade.bladesmasher.monster.factory.concrete.LeechFactory;
import com.blade.bladesmasher.monster.factory.concrete.ZombieFactory;
import com.blade.bladesmasher.monster.observer.MonsterEvent;
import com.blade.bladesmasher.monster.observer.MonsterObserver;
import com.blade.bladesmasher.player.Player;

public class GameScreen implements Screen, MonsterObserver {
	
	private final float MONSTER_SIZE = Gdx.graphics.getWidth()*0.3000f;
	private final float WIDTH = Gdx.graphics.getWidth();
	private final float HEIGHT = Gdx.graphics.getHeight();
	private final float PAUSE_ICON_SIZE = Gdx.graphics.getWidth()*0.100f;
	private float timeModeTime = 60;
	private LifeBarPanel lifeBarPanel;
	private Sprite backgroundActor;
	private int combo;
	private int score;
	private Label scoreLabel;
	private Label comboLabel;
	private Label timeLabel;
	private Array<IMonster> monsters;
	private int attempts;
	private Group backgroundLayer;
	private Group foregroundLayer;
	private Stage stage;
	private int comboValue = 1;
	
	private boolean isPaused = false;
	private float monsterSpeed;
	
	private long lastMonsterSpawnTime;
	private GameMode currentGameMode;
	private Texture backgroundTexture;
	
	private boolean isReadyToZombieMonsters = false;
	
	private LeechFactory leechFactory;
	private ZombieFactory zombieFactory;
	public GameScreen(SpriteBatch batch, GameMode gameMode) {
		currentGameMode = gameMode;
		monsterSpeed = 3f;
		attempts = 3;
		combo = 0;
		score = 0;
		monsters = new Array<IMonster>();
		backgroundTexture = new Texture("data/dirt_road.jpg");
		leechFactory = new LeechFactory();
		zombieFactory = new ZombieFactory();
        stage = new Stage(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), false, batch);      
        initStageLayers();  
        initBackground(new TextureRegion(backgroundTexture));
        if(currentGameMode != GameMode.TIME_MODE) {
        	initLifeBar();
        }
        initMonsterLabel();
        spawnMonster();
        initPauseButton();
        if(currentGameMode == GameMode.TIME_MODE) {
			initTimeLabel();
        }
    }
	 
	@Override
	public void render(float delta) {
 		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT); 
 		if(currentGameMode == GameMode.TIME_MODE) {
 			timeModeTime -= delta;
	 		updateTimeLabel((int)timeModeTime);
	 		System.out.println("elapsed time: "+String.valueOf((int)timeModeTime));
	 		  if(timeModeTime <= 0) {
	 		    gameOver();
	 		}
 		}
 		if(TimeUtils.nanoTime() - lastMonsterSpawnTime > 1000000000) {
 			spawnMonster();
 		}
 		Timer.schedule(new Task(){
		    @Override
		    public void run() {
		    	//time out
		    	
		    	isReadyToZombieMonsters = true;
		    }
		}, 10f);
		updateMonsters(delta);
        stage.act(delta);
        stage.draw();
	}
	
	private void updateMonsters(float deltaTime) {
		if(monsters != null) {
			for(IMonster monster : monsters) {
				monster.increaseMonsterStateTime(deltaTime);
			}
		}
	}	
	
	private void generateBonus(float x, float y) {
		float bonusDropChance = MathUtils.random() * 100;
		if(bonusDropChance < 10) {
			final Sprite bonus = new Sprite(new TextureRegion(new Texture("data/x2.png")));
			bonus.setSize(MONSTER_SIZE/2, MONSTER_SIZE/2);
			bonus.setPosition(x, y);
			bonus.addListener(new BonusClickListener());
			bonus.addAction(sequence(fadeOut(MathUtils.random(1.9f, 3.5f)), run(new Runnable() {
				public void run() {	
						
				}
			})));
			foregroundLayer.addActor(bonus);
			Timer.schedule(new Task(){
			    @Override
			    public void run() {
			    	//time out	
			    	bonus.remove();
			    }
			}, 3f);
		}
	}
	
	class BonusClickListener extends ClickListener {
		public void clicked(final com.badlogic.gdx.scenes.scene2d.InputEvent event, float x, float y) {
			comboValue = 2;
			Timer.schedule(new Task(){
			    @Override
			    public void run() {
			    	//time out	
			    	comboValue = 1;
			    }
			}, 5f);
			((Sprite)event.getTarget()).remove();
		};
	}
	
	private void spawnMonster() {		
		if(!isPaused) {
			final IMonster monster;
			if(isReadyToZombieMonsters) {
				monster = zombieFactory.createMonster();
			} else {
				monster = leechFactory.createMonster();
			}
			monster.addMonsterObserver(this);
			monster.setSize(MONSTER_SIZE, MONSTER_SIZE);
			monster.setPosition(MathUtils.random(0, WIDTH-MONSTER_SIZE), HEIGHT);
			monster.addAction(sequence(moveBy(0, -HEIGHT, monsterSpeed), run(monster)));
			monster.addListener(new MonsterClickListener());
			monsters.add(monster);
			foregroundLayer.addActor(monster);
			lastMonsterSpawnTime = TimeUtils.nanoTime() - 520000000;
			if(monsterSpeed > 2.5f) {
				monsterSpeed-=0.1f;
			}
		}	
    }
	
	private void initPauseButton() {
		Sprite pauseButton = new Sprite(new TextureRegion(new Texture("data/pause_icon.png")));
		pauseButton.setSize(PAUSE_ICON_SIZE, PAUSE_ICON_SIZE);
		pauseButton.setPosition(WIDTH/1.2f, HEIGHT*0.0800f);
		pauseButton.addListener(new PauseButtonListener());
		foregroundLayer.addActor(pauseButton);
	}
	
	private void initMonsterLabel() {
		LabelStyle lStyle = new LabelStyle();
		lStyle.font = new BitmapFont();		
		lStyle.fontColor = new Color(1f, 1f,1f, 1f);
		scoreLabel = new Label("Score: 0", lStyle);
		scoreLabel.setPosition(Gdx.graphics.getWidth()/8, Gdx.graphics.getHeight()-30);
		foregroundLayer.addActor(scoreLabel);
	}
	
	private void updateMonsterLabel(int score) {
		scoreLabel.setText("Score: "+score);
	}
	
	private void initTimeLabel() {
		LabelStyle lStyle = new LabelStyle();
		lStyle.font = new BitmapFont();		
		lStyle.fontColor = new Color(1f, 1f,1f, 1f);
		timeLabel = new Label("Time elapsed: 0", lStyle);
		timeLabel.setPosition(Gdx.graphics.getWidth()/8, Gdx.graphics.getHeight()/10);
		foregroundLayer.addActor(timeLabel);
	}
	
	private void updateTimeLabel(int time) {
		timeLabel.setText("Time: "+time);
	}
	
	private void initStageLayers() {
		backgroundLayer  = new Group();
		backgroundLayer.setBounds(-0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		foregroundLayer = new Group();
		foregroundLayer.setBounds(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		stage.addActor(backgroundLayer);
		stage.addActor(foregroundLayer);
	}
	
	private void initLifeBar() {
		lifeBarPanel = new LifeBarPanel(50, 50);
		lifeBarPanel.setPosition(Gdx.graphics.getWidth()/1.6f, Gdx.graphics.getHeight()-80);
		stage.addActor(lifeBarPanel);
	}
	
	private void updateLifeBar(int attempts) {
		if(attempts == 2) {
			lifeBarPanel.removeHeart1();
		} else if(attempts == 1) {
			lifeBarPanel.removeHeart2();
		} else if(attempts <= 0) {
			lifeBarPanel.removeHeart3();
			Timer.schedule(new Task(){
			    @Override
			    public void run() {
			    	//time out
			    	gameOver();
			    }
			}, 0.3f);
		}
	}
	
	private void gameOver() {
		Player.getInstance().setMoney(score/20);
    	Player.getInstance().savePlayer();
    	BladeSmasher.getInstance().showLoseScreen();
	}

	private void initBackground(TextureRegion textureRegion) {
		backgroundActor = new Sprite(textureRegion);
		backgroundActor.setSize(WIDTH, HEIGHT);
		backgroundLayer.addActor(backgroundActor);
	}
	
	private void updateComboLabel(int combo) {
		if(comboLabel != null) {
			comboLabel.setText("Combo: "+combo+"X");
		} else {
			if(combo %2 == 0) {
				LabelStyle lStyle = new LabelStyle();
				lStyle.font = new BitmapFont();
				lStyle.fontColor = new Color(1f, 1f,1f, 1f);
				comboLabel = new Label("Combo: "+combo+"X", lStyle);
				comboLabel.setPosition(Gdx.graphics.getWidth()/8, Gdx.graphics.getHeight()-70);
				foregroundLayer.addActor(comboLabel);
			}
		}
	}
	
	private void playSmashSound() {
		Music smashSound = Gdx.audio.newMusic(Gdx.files.internal("data/smash-sound.mp3"));
		smashSound.play();
	}
	
	class MonsterClickListener extends ClickListener {
		public void clicked(final com.badlogic.gdx.scenes.scene2d.InputEvent event, float x, float y) {
			((IMonster)event.getTarget()).die();	
		};
	}
	
	class SawBonusClickListener extends ClickListener {
		public void clicked(final com.badlogic.gdx.scenes.scene2d.InputEvent event, float x, float y) {
			 ((Sprite)event.getTarget()).addAction(rotateBy(360, 5f));	
		};
	}
	
	class PauseButtonListener extends ClickListener {
		public void clicked(final com.badlogic.gdx.scenes.scene2d.InputEvent event, float x, float y) {
			if(isPaused) {
				isPaused = false;
				for(IMonster monster : monsters) {
					monster.setTouchable(Touchable.enabled);
					monster.addAction(sequence(moveBy(0, -HEIGHT, monsterSpeed), run(monster)));
					monster.continueAnimation();
					monster.setColor(Color.WHITE);
					backgroundActor.setColor(Color.WHITE);
				}
			} else {
				isPaused = true;
				for(IMonster monster : monsters) {
					monster.setTouchable(Touchable.disabled);
					monster.removeAction(monster.getActions().get(0));
					monster.stopAnimation();
					monster.setColor(1,1,1,0.5f);
					backgroundActor.setColor(1,1,1,0.5f);
				}
			}
		};
	}
	
	private void reset() {
		attempts = 3;
		score = 0;
		if(currentGameMode != GameMode.TIME_MODE) {
			updateLifeBar(attempts);
		}
		updateMonsterLabel(score);
		for(IMonster monster : monsters) {
			monster.remove();
		}
		spawnMonster();
	}
	
	@Override
	public void resize(int width, int height) {
		
	}

	@Override
	public void show() {
		Gdx.input.setInputProcessor(stage);
		reset();
	}

	@Override
	public void hide() {
		
	}

	@Override
	public void pause() {
		
	}

	@Override
	public void resume() {
		
	}

	@Override
	public void dispose() {
		stage.dispose();
	}
	
	@Override
	public void monsterEventHappened(final IMonster monster, MonsterEvent event) {
		if(event == MonsterEvent.OUT_OF_RANGE) {
			combo = 0;
			if(comboLabel != null) {
				comboLabel.remove();
				comboLabel = null;
			}
			monsters.removeValue(monster, true);
			if(currentGameMode != GameMode.TIME_MODE) {
				attempts--;
				updateLifeBar(attempts);
			}
		} else if(event == MonsterEvent.DIED) {
			monster.setTouchable(Touchable.disabled);
			playSmashSound();
			monster.setMonsterState(MonsterState.DYING);
			monsters.removeValue(monster, true);
			monster.removeAction(monster.getActions().get(0));
			monster.addAction(sequence(fadeOut(1f), run(new Runnable() {
				public void run() {	
					monster.remove();		
				}
			})));			
			generateBonus(monster.getX()+(monster.getWidth()/2), monster.getY());
			score+=monster.getScoreForMonster();
			combo+=comboValue;
			updateComboLabel(combo);
			updateMonsterLabel(score);	
		}
	}
}

package com.blade.bladesmasher.actors;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class Sprite extends Actor{
	private TextureRegion region;
	public Sprite(TextureRegion region) {
		this.region = region;
	}
	
	public void setRegion(TextureRegion region) {
		this.region = region;
	}
	
	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		batch.setColor(getColor());
		batch.draw(region, getX(), getY(), getWidth(), getHeight());
	}

	public TextureRegion getRegion() {
		return region;
	}
}

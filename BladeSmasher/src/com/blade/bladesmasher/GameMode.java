package com.blade.bladesmasher;

public enum GameMode {
	STORY_MODE,
	SURVIVE_MODE,
	TIME_MODE
}
